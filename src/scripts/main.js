import GoogleMapsLoader from 'google-maps';
import jsb from 'js-beautify';
import '../styles/main.scss';

const BRTAkAttr = 'Kaartgegevens &copy; <a href="cbs.nl">CBS</a>, <a href="kadaster.nl">Kadaster</a>, <a href="openstreetmap.org">OpenStreetMap contributors</a>'; 
const baseTileUrl = 'http://tiles.energielabelatlas.nl/v2/osm';

const mapFns = {
  leaflet: function makeLeafletMap() {
    //create a map
    let map = L.map('nlmap', {
      center: [52.2112, 5.9699],
      layers: [
        new L.tileLayer(
          `${baseTileUrl}/{z}/{x}/{y}.png`,
          {
            attribution: BRTAkAttr
          }
        )
      ],
      zoom: 10
    });
    return map;
  },
  google: function makeGoogleMap() {
    GoogleMapsLoader.KEY = 'AIzaSyAYCu4ZY9tssUK4luavRsNyTirXdEnC3qw';
    return GoogleMapsLoader.load(function(google){
      let map = new google.maps.Map(document.getElementById('nlmap'), {
        center: {lat: 52.2112, lng: 5.9699},
        zoom: 10
      });
      //basemap from amsterdam for now
      let ElaMap = new google.maps.ImageMapType({
        getTileUrl: function (coord, zoom) {
          let url = `${baseTileUrl}/${zoom}/${coord.x}/${coord.y}.png`;
          return url;
        },
        tileSize: new google.maps.Size(256, 256),
        isPng: true,
        name: 'Amsterdam',
        maxZoom: 22,
        minZoom: 8
      });

      function AttributionControl(controlDiv) {
        // Set CSS for the control border.
        let controlUI = document.createElement('div');
        controlUI.style.backgroundColor = '#fff';
        controlUI.style.opacity = '0.7';
        controlUI.style.border = '2px solid #fff';
        controlUI.style.cursor = 'pointer';
        controlDiv.appendChild(controlUI);

        // Set CSS for the control interior.
        let controlText = document.createElement('div');
        controlText.style.color = 'rgb(25,25,25)';
        controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
        controlText.style.fontSize = '10px';
        controlText.innerHTML = BRTAkAttr;
        controlUI.appendChild(controlText);
        return controlDiv;
      }

      map.mapTypes.set('Energielabelatlas', ElaMap);
      map.setOptions({
        mapTypeControl: false,

      });
      map.setMapTypeId('Energielabelatlas');
      // Create the DIV to hold the control and call the CenterControl()
      // constructor passing in this DIV.
      let centerControlDiv = document.createElement('div');
      let centerControl = new AttributionControl(centerControlDiv, map);

      centerControlDiv.index = 1;
      map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(centerControl);

      return map;
    });

  },
  openlayers: function makeOLMap() {
    let map = new ol.Map({
      layers: [
        new ol.layer.Tile({
          source: new ol.source.XYZ({
            url: `${baseTileUrl}/{z}/{x}/{y}.png`,
            attributions: [
              new ol.Attribution({
                html: BRTAkAttr
              })
            ]
          })
        })
      ],
      view: new ol.View({
        center: [664197,6838137],
        zoom: 10
      }),
      target: 'nlmap'

    });
    return map;
  },
  mapbox: function makeMapBoxMap() {
    const map = L.mapbox.map('nlmap', null, {
    })
    .setView([52.2093694276, 5.97074558341], 10);

    //const layer = L.mapbox.tileLayer(tilejson, {tms: true});
    const layer = L.tileLayer(`${baseTileUrl}/{z}/{x}/{y}.png`,
      {
        tms: false,
        attribution: BRTAkAttr
      }
    );
    layer.addTo(map);

    return map;
  }
};
let initializedmap;

//create map, replacing previous if applicable
function replaceMap(key){
  if (typeof initializedmap !== 'undefined'){
    if ('remove' in initializedmap) {
      initializedmap.remove();
    }
  }
  initializedmap = mapFns[key]();
}


//make map and write source below
function render(key, printTarget){
  document.getElementById('nlmap').innerHTML = '';
  replaceMap(key);
  let fnSrc = mapFns[key].toString();
  fnSrc = fnSrc.replace(/^.*{/, '');
  fnSrc = fnSrc.replace(/}$/, '');
  printTarget.innerHTML = jsb(fnSrc, {indent_size: 2});
}

function highlightButton(e){
  let el = e.target;
  if (el.className === 'btn selected') {
    let all = [].slice.call(el.parentElement.children);
    all.forEach( x => x.className = 'btn');
  } else {
    let siblings = [].slice.call(el.parentElement.children)
      .filter( x => x !== e);
    siblings.forEach( x => x.className = 'btn');
    el.className = 'btn selected';
  }
}

//wire up the clicks to outputs
window.onload = function() {
  let printTarget = document.getElementById('code');
  Object.keys(mapFns).forEach(key => {
    const el = document.getElementById(key);
    el.addEventListener('click', (e) => {
      render(key, printTarget);
      highlightButton(e);
    });
  });
  //start with leaflet on pageload
  render('leaflet', printTarget);
  highlightButton({target: document.getElementById('leaflet')});
}();
